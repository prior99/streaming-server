# streaming-server

This is a rust server accepting websocket connections of clients streaming opus encoded audio.
It will relay the audio chunks to all client connected to the same "room".

## REST API

Merely one API endpoint exists:

```
curl http://127.0.0.1:8080/create
```

It will return a pair of speaker and listener ids:

```
{
    "listener": "a2a451273d",
    "speaker": "f4e5739718"
}
```

A room is simply a set of randomly generated id and a matching hashed version of it.

The hashed version is used as the listener id, so it is impossible to guess the speaker's id from the listener id.

Rooms are in no way persisted, no database or persistence is needed.

## Usage

Simply start the server using the `start` subcommand. It is possible to specify a port using `-p` as well as a host using `-h`.

```
./streaming-server start -p 8080 -h 0.0.0.0
```

## Contributors

 - Frederick Gnodtke
