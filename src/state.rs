use actix::prelude::Addr;
use crate::negotiator::Negotiator;

pub struct State {
    pub addr: Addr<Negotiator>,
}
