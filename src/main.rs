extern crate actix;
extern crate actix_web;
#[macro_use] extern crate clap;
extern crate futures;
#[macro_use] extern crate log;
#[macro_use] extern crate serde_derive;
extern crate serde;
extern crate serde_json;
extern crate sha2;
extern crate simplelog;
extern crate uuid;

mod negotiator;
mod state;
mod listener;
mod messages;
mod room;
mod server;
mod speaker;

use simplelog::{SimpleLogger, LevelFilter, Config};
use crate::server::start_server;

fn main() {
    use clap::App;
    let yml = load_yaml!("commandline.yml");
    let mut app = App::from_yaml(yml);
    let matches = app.clone().get_matches();
    let log_level = if matches.is_present("verbose") {
        LevelFilter::Debug
    } else {
        LevelFilter::Info
    };
    // Setup logging.
    if let Err(error) = SimpleLogger::init(log_level, Config::default()) {
        println!("Unable to setup logging: {}", error);
        println!("Terminating.");
        return;
    }
    match matches.subcommand() {
        ("start", Some(start_matches)) => {
            let port = start_matches.value_of("port").expect("No port specified.");
            let host = start_matches.value_of("host").expect("No host specified.");
            let address = format!("{}:{}", host, port);
            start_server(&address);
        },
        _ => {
            if let Err(error) = app.print_long_help() {
                error!("Unable to print help: {}", error);
            }
        },
    };
}
