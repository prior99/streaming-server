use actix::*;
use actix_web::ws::{WebsocketContext, Message, ProtocolError};
use crate::state::State;
use crate::messages::{ListenerConnected, ListenerDisconnected, AudioData, Info};
use uuid::Uuid;

/// A client listening to a stream.
pub struct ClientListener {
    /// This client's unique id.
    pub id: Option<Uuid>,
    pub listener_id: String,
}

impl Actor for ClientListener {
    type Context = WebsocketContext<Self, State>;

    /// Called when the client connected.
    fn started(&mut self, ctx: &mut Self::Context) {
        let addr = ctx.address();
        // Send the `ListenerConnected` message to register with the negotiator.
        ctx.state().addr
            .send(ListenerConnected {
                addr: addr.clone(),
                listener_id: self.listener_id.to_string(),
            })
            .into_actor(self)
            .then(|response, actor, ctx| {
                // Store the ID of the client or shut the client down.
                match response {
                    Ok(Ok(id)) => { actor.id = Some(id); },
                    _ => ctx.stop(),
                };
                fut::ok(())
            })
            .wait(ctx);
    }

    /// Called when the client disconnected.
    fn stopping(&mut self, ctx: &mut Self::Context) -> Running {
        // Unregister from the negotiator using a `ListenerDisconnected` message.
        if let Some(id) = self.id {
            ctx.state().addr.do_send(ListenerDisconnected { id });
        }
        Running::Stop
    }
}

impl Handler<Info> for ClientListener {
    type Result = ();

    fn handle(&mut self, msg: Info, ctx: &mut Self::Context) {
        // Send the info struct to the client to inform it about the audio configuration.
        match serde_json::to_string(&msg) {
            Ok(info_json) => ctx.text(info_json),
            Err(error) => error!("Unable to stringify message: {}", error),
        }
    }
}

impl Handler<AudioData> for ClientListener {
    type Result = ();

    fn handle(&mut self, msg: AudioData, ctx: &mut Self::Context) {
        ctx.binary(msg.data);
    }
}

impl ClientListener {
    pub fn new(listener_id: &str) -> ClientListener {
        ClientListener {
            id: None,
            listener_id: listener_id.to_string(),
        }
    }
}

impl StreamHandler<Message, ProtocolError> for ClientListener {
    fn handle(&mut self, msg: Message, ctx: &mut Self::Context) {
        match msg {
            Message::Ping(msg) => ctx.pong(&msg),
            Message::Text(text) => ctx.text(text),
            Message::Binary(bin) => ctx.binary(bin),
            _ => (),
        }
    }
}
