use sha2::{Sha512, Digest};
use uuid::Uuid;

#[derive(Clone, Serialize)]
pub struct Room {
    pub speaker: String,
    pub listener: String,
}

impl Room {
    pub fn new() -> Room {
        let id = Uuid::new_v4();
        let speaker = id.to_simple().to_string()[0..10].to_string();
        let hash = Sha512::digest(speaker.as_bytes());
        let listener = format!("{:x}", hash)[0..10].to_string();

        Room {
            speaker,
            listener,
        }
    }

    pub fn from_speaker(speaker: &str) -> Room {
        let hash = Sha512::digest(speaker.as_bytes());
        let listener = format!("{:x}", hash)[0..10].to_string();

        Room {
            speaker: speaker.to_string(),
            listener,
        }
    }
}
