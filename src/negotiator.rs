use actix::prelude::{Context, Handler};
use actix::{Actor, Addr};
use std::collections::{HashMap};
use uuid::Uuid;
use crate::messages::{Info, SpeakerInfo, ListenerConnected, ListenerDisconnected, AudioData, SpeakerConnected, SpeakerDisconnected};
use crate::listener::ClientListener;
use crate::room::Room;

struct ClientListenerWrapper {
    listener_id: String,
    addr: Addr<ClientListener>,
}

struct ClientSpeakerWrapper {
    speaker_id: String,
}

/// Negotiates and broadcasts messages from speakers to listeners.
pub struct Negotiator {
    listeners: HashMap<Uuid, ClientListenerWrapper>,
    speakers: HashMap<Uuid, ClientSpeakerWrapper>,
    infos: HashMap<String, Info>,
}

impl Default for Negotiator {
    fn default() -> Negotiator {
        Negotiator {
            listeners: HashMap::new(),
            speakers: HashMap::new(),
            infos: HashMap::new(),
        }
    }
}

impl Actor for Negotiator {
    type Context = Context<Self>;
}

impl Handler<SpeakerConnected> for Negotiator {
    type Result = Result<Uuid, ()>;

    fn handle(&mut self, msg: SpeakerConnected, _: &mut Context<Self>) -> Self::Result {
        // Assign the new speaker an ID.
        let id = Uuid::new_v4();
        self.speakers.insert(id, ClientSpeakerWrapper {
            speaker_id: msg.speaker_id.clone(),
        });
        info!("Speaker with id {} connected to room with speaker id {}.", id, msg.speaker_id);
        Ok(id)
    }
}

impl Handler<SpeakerDisconnected> for Negotiator {
    type Result = ();

    fn handle(&mut self, msg: SpeakerDisconnected, _: &mut Context<Self>) -> Self::Result {
        if let Some(speaker) = self.speakers.remove(&msg.id) {
            let room = Room::from_speaker(&speaker.speaker_id);
            self.infos.remove(&room.listener);
            info!("Speaker with id {} disconnected.", msg.id);
        }
    }
}

impl Handler<ListenerConnected> for Negotiator {
    type Result = Result<Uuid, ()>;

    fn handle(&mut self, msg: ListenerConnected, _: &mut Context<Self>) -> Self::Result {
        // Assign the new listener an ID.
        let id = Uuid::new_v4();
        self.listeners.insert(id, ClientListenerWrapper {
            addr: msg.addr.clone(),
            listener_id: msg.listener_id.clone(),
        });
        // If information about the audiodata has already been received by the speaker,
        // send it to the new listener initially.
        if let Some(info) = self.infos.get(&msg.listener_id) {
            msg.addr.do_send(info.clone());
        }
        info!("Listener with id {} connected to room with listener id {}.", id, msg.listener_id);
        Ok(id)
    }
}

impl Handler<ListenerDisconnected> for Negotiator {
    type Result = ();

    fn handle(&mut self, msg: ListenerDisconnected, _: &mut Context<Self>) -> Self::Result {
        self.listeners.remove(&msg.id);
        info!("Listener with id {} disconnected.", msg.id);
    }
}

impl Handler<AudioData> for Negotiator {
    type Result = ();

    fn handle(&mut self, msg: AudioData, _: &mut Context<Self>) -> Self::Result {
        match self.speakers.get(&msg.client_id) {
            Some(speaker) => {
                let room = Room::from_speaker(&speaker.speaker_id);
                // If no information about the audiodata has been received from the speaker yet,
                // discard the audio data.
                if self.infos.get(&room.listener).is_some() {
                    // Broadcast the audio data to all listeners.
                    self.listeners.iter().for_each(|(_, listener)| {
                        if listener.listener_id == room.listener {
                            listener.addr.do_send(msg.clone());
                        }
                    });
                }
            },
            None => { error!("Received audio data message for unknown speaker with id {}.", msg.client_id); },
        }
    }
}

impl Handler<SpeakerInfo> for Negotiator {
    type Result = ();

    fn handle(&mut self, msg: SpeakerInfo, _: &mut Context<Self>) -> Self::Result {
        match self.speakers.get(&msg.client_id) {
            Some(speaker) => {
                let room = Room::from_speaker(&speaker.speaker_id);
                self.infos.insert(room.listener.clone(), msg.info.clone());
                // Broadcast the info to all listeners.
                self.listeners.iter().for_each(|(_, listener)| {
                    if listener.listener_id == room.listener {
                        listener.addr.do_send(msg.info.clone());
                    }
                });
            },
            None => { error!("Received info message for unknown speaker with id {}.", msg.client_id); },
        }
    }
}
