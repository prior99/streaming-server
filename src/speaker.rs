use actix::*;
use actix_web::ws::{WebsocketContext, Message, ProtocolError};
use crate::state::State;
use crate::messages::{AudioData, SpeakerInfo, SpeakerConnected, SpeakerDisconnected, Info};
use crate::room::Room;
use uuid::Uuid;

pub struct ClientSpeaker {
    bytes_received: usize,
    info: Option<Info>,
    room: Room,
    id: Option<Uuid>,
}

impl Actor for ClientSpeaker {
    type Context = WebsocketContext<Self, State>;

    /// Called when the client connected.
    fn started(&mut self, ctx: &mut Self::Context) {
        // Send the `Connect` message to register with the negotiator.
        ctx.state().addr
            .send(SpeakerConnected {
                speaker_id: self.room.speaker.clone(),
            })
            .into_actor(self)
            .then(|response, actor, ctx| {
                // Store the ID of the client or shut the client down.
                match response {
                    Ok(Ok(id)) => {
                        actor.id = Some(id)
                    },
                    _ => ctx.stop(),
                };
                fut::ok(())
            })
            .wait(ctx);
    }

    /// Called when the client disconnected.
    fn stopping(&mut self, ctx: &mut Self::Context) -> Running {
        // Unregister from the negotiator using a `Disconnect` message.
        if let Some(id) = self.id {
            ctx.state().addr.do_send(SpeakerDisconnected { id });
        }
        Running::Stop
    }
}

impl ClientSpeaker {
    pub fn new(speaker_id: &str) -> ClientSpeaker {
        ClientSpeaker {
            bytes_received: 0,
            info: None,
            room: Room::from_speaker(speaker_id),
            id: None,
        }
    }
}

impl StreamHandler<Message, ProtocolError> for ClientSpeaker {
    fn handle(&mut self, msg: Message, ctx: &mut Self::Context) {
        match msg {
            Message::Ping(msg) => ctx.pong(&msg),
            Message::Text(text) => {
                // Text messages are currently always `Info` structs, informing the
                // listener about the audio data. Store it and send it to the negotiator.
                let info: Result<Info, _> = serde_json::from_str(&text);
                self.info = match info {
                    Err(_) => {
                        info!("Received malformed info struct.");
                        None
                    },
                    Ok(value) => {
                        match self.id {
                            Some(id) => {
                                info!("Received info struct with samplerate of {}Hz and {} channels from client {}.", value.sample_rate, value.channels, id);
                                ctx.state().addr.do_send(SpeakerInfo {
                                    client_id: id.clone(),
                                    info: value.clone(),
                                });
                            },
                            None => warn!("Received info struct from client with no id associated."),
                        };
                        Some(value)
                    }
                };
            },
            Message::Binary(data) => {
                // Binary messages represent the opus stream.
                self.bytes_received += data.len();
                match self.id {
                    Some(id) => {
                        ctx.state().addr.do_send(AudioData {
                            data: data.clone(),
                            client_id: id.clone(),
                        });
                    },
                    None => warn!("Received audio data from client with no id associated."),
                };
            },
            _ => (),
        }
    }
}
