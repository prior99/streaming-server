use actix::Arbiter;
use actix_web::{App, HttpRequest, HttpResponse, Responder};
use actix_web::middleware::cors::Cors;
use actix_web::server::HttpServer;
use actix_web::ws::{start};
use crate::listener::ClientListener;
use crate::speaker::ClientSpeaker;
use crate::state::State;
use crate::negotiator::Negotiator;
use crate::room::Room;

pub fn speak(request: &HttpRequest<State>) -> impl Responder {
    match request.match_info().get("id") {
        Some(id) => {
            match start(request, ClientSpeaker::new(id)) {
                Ok(response) => response,
                Err(error) => {
                    error!("Failed to connect websocket: {}", error);
                    HttpResponse::InternalServerError().finish()
                }
            }
        },
        None => HttpResponse::NotFound().finish(),
    }
}

pub fn listen(request: &HttpRequest<State>) -> impl Responder {
    match request.match_info().get("id") {
        Some(id) => {
            match start(request, ClientListener::new(id)) {
                Ok(response) => response,
                Err(error) => {
                    error!("Failed to connect websocket: {}", error);
                    HttpResponse::InternalServerError().finish()
                }
            }
        },
        None => HttpResponse::NotFound().finish(),
    }
}

pub fn create(_request: &HttpRequest<State>) -> impl Responder {
    let room = Room::new();
    info!("Room with id {} created.", room.speaker);
    HttpResponse::Ok().json(&room)
}

pub fn start_server(address: &str) {
    // Setup Actix.
    let sys = actix::System::new("streaming-server");
    let server = Arbiter::start(|_| Negotiator::default());
    // Start HTTP server.
    match HttpServer::new(move || {
        let state = State {
            addr: server.clone(),
        };
        // Register routes.
        App::with_state(state)
            .configure(|app| {
                Cors::for_app(app)
                    .resource("/speak/{id}", |r| r.f(speak))
                    .resource("/listen/{id}", |r| r.f(listen))
                    .resource("/create", |r| r.f(create))
                    .register()
            })
    }).bind(address) {
        Ok(server) => {
            server.start();
            info!("Started server on {}", address);
        },
        Err(error) => error!("Unable to start server: {}", error),
    };
    // Start Actix.
    let _ = sys.run();
}

