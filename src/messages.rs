use actix_web::Binary;
use actix::{Message, Addr};
use crate::listener::ClientListener;
use uuid::Uuid;
use std::result::Result;

#[derive(Message, Deserialize, Serialize, Clone)]
pub struct Info {
    pub sample_rate: usize,
    pub channels: usize,
}

/// Inform the client or server about the samplerate and channels.
#[derive(Message, Clone)]
pub struct SpeakerInfo {
    pub info: Info,
    pub client_id: Uuid,
}

/// A new listening client connected.
pub struct ListenerConnected {
    pub addr: Addr<ClientListener>,
    pub listener_id: String,
}

impl Message for ListenerConnected {
    type Result = Result<Uuid, ()>;
}

/// A listening client disconnected.
#[derive(Message)]
pub struct ListenerDisconnected {
    pub id: Uuid,
}

/// Audio data has been received by a speaker.
#[derive(Message, Clone)]
pub struct AudioData {
    pub client_id: Uuid,
    pub data: Binary,
}


/// A new speaking client connected.
pub struct SpeakerConnected {
    pub speaker_id: String,
}

impl Message for SpeakerConnected {
    type Result = Result<Uuid, ()>;
}

/// A speaking client disconnected.
#[derive(Message)]
pub struct SpeakerDisconnected {
    pub id: Uuid,
}
